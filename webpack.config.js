const webpackMerge = require("webpack-merge");
const singleSpaDefaults = require("webpack-config-single-spa-react-ts");

module.exports = (webpackConfigEnv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: "lsegala",
    projectName: "content",
    webpackConfigEnv,
    module: {
      rules: [
        {
          test: /.*\.png$/,
          exclude: /(node_modules)/,
          use: [
            {
              loader: "url-loader?limit=200000",
            },
          ],
        },
      ],
    },
  });

  return webpackMerge.smart(defaultConfig, {
    // modify the webpack config however you'd like to by adding to this object
  });
};
